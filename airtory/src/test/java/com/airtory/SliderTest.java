package com.airtory;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SliderTest {
    private static WebDriver driver;
    public WebDriverWait wait ;
    @BeforeTest
    public void setUp()
    {
        //System.setProperty("webdriver.chrome.driver","/Users/ria/Code/Library/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        //options.setBinary("/usr/local/bin/chromium");
        options.addArguments("headless");
        options.addArguments("disable-infobars");
        options.addArguments("chrome.switches", "--disable-extensions","--disable-gpu","--no-sandbox");

		driver = new ChromeDriver(options);

		driver.get("https://studio.airtory.com/ad-preview/#7ae61257c72bd7e888c8363621135620/mobile_portrait");
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, Duration.ofSeconds(60));
    }

    By console = By.xpath("/html/body/div[1]/ul/li[3]/div[2]/div");
    By adImage = By.xpath("/html/body/main/section/div[12]/div/div/div/div[2]/div/div[1]/img");
    By table = By.className("ay-console__table");
    By inConsoleView = By.xpath("/html/body/div[1]/ul/li[3]/div[1]/div/div[2]/div[1]/table/tbody/tr[2]/td[1]");
    By clearButton = By.xpath("/html/body/div[1]/ul/li[3]/div[1]/div/div[2]/div[2]/div[2]/button");
    By CheckAdIsThere = By.xpath("//*[@id=\"air-294-container\"]/div");

    By openSlide = By.xpath("/html/body/div[1]/ul/li[3]/div[1]/div/div[2]/div[1]/table/tbody/tr[3]/td[1]");

    By LeftSwipe = By.xpath("/html/body/div[1]/ul/li[3]/div[1]/div/div[2]/div[1]/table/tbody/tr[6]/td[1]");
    By RightSwipe = By.xpath("/html/body/div[1]/ul/li[3]/div[1]/div/div[2]/div[1]/table/tbody/tr[8]/td[1]");

    By Image1 = By.xpath("//*[@id=\"air-294-container\"]/div/div[1]/div/div/div/div[1]/img[1]");
    By Image2 = By.xpath("//*[@id=\"air-294-container\"]/div/div[1]/div/div/div/div[2]/img[1]");
    By Image3 = By.xpath("//*[@id=\"air-294-container\"]/div/div[1]/div/div/div/div[3]/img[1]");
    By Image4 = By.xpath("//*[@id=\"air-294-container\"]/div/div[1]/div/div/div/div[4]/img[1]");

    By smallImage1 = By.xpath("//*[@id=\"air-294-container\"]/div/div[2]/div/div[1]/img");
    By smallImage2 = By.xpath("//*[@id=\"air-294-container\"]/div/div[2]/div/div[2]/img");
    By smallImage3 = By.xpath("//*[@id=\"air-294-container\"]/div/div[2]/div/div[3]/img");
    By smallImage4 = By.xpath("//*[@id=\"air-294-container\"]/div/div[2]/div/div[4]/img");

    By minimizeSlider = By.xpath("//*[@id=\"air-294-container\"]/div/span[2]");
    
    By minimizevalue = By.xpath("/html/body/div[1]/ul/li[3]/div[1]/div/div[2]/div[1]/table/tbody/tr[7]/td[1]");
    By closeConsole = By.xpath("/html/body/div[1]/ul/li[3]/div[2]/div");
    By closeAd = By.xpath("//*[@id=\"air-294-container\"]/div/span[1]");
    By AfterAdClose = By.xpath("/html/body/main/section/div[8]/article/div");
   
    @Test
    public void checkIfAdIsThere() throws InterruptedException {
        System.out.println("In First function: Slider PAge");

        // check if ad is there.
        driver.switchTo().frame("native");

        wait.until(ExpectedConditions.visibilityOfElementLocated(CheckAdIsThere));
        boolean adDisplay = driver.findElement(CheckAdIsThere).isDisplayed();
        // System.out.println(adDisplayed);
        if (!adDisplay) {
            System.out.println("Ad is not there");
            driver.quit();
        }

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        System.out.println("Ad is there");
        driver.switchTo().parentFrame();

        // clicking console to open it
        driver.findElement(console).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

        // Checking if View is there or not in console tab
        System.out.println("Console View");

        wait.until(ExpectedConditions.visibilityOfElementLocated(inConsoleView));

        System.out.println("Out of First function: Slider PAge");
    }

    @Test(dependsOnMethods= {"checkIfAdIsThere"})
    public void AdImageClick() throws InterruptedException {
		System.out.println("In Second function: AdImageClick");

		// Clicking image to enlarge it
		System.out.println("Ad Image click");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("native"));

		//driver.switchTo().frame("native");

		System.out.println("Switched to iframe");
		Thread.sleep(5000);
		Thread.sleep(5000);

		// to enlarge the small carousel.

		By smallCarou = By.xpath("//*[@id=\"air-294-container\"]/div/div[2]");

		wait.until(ExpectedConditions.presenceOfElementLocated(smallImage2));
		wait.until(ExpectedConditions.elementToBeClickable(smallCarou));
		Thread.sleep(5000);

		driver.findElement(smallCarou).click();
		Thread.sleep(5000);
	
		System.out.println("After enlarging");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		// -------------------------------------------------------
		// to check if action is logged in console
		driver.switchTo().parentFrame();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		wait.until(ExpectedConditions.presenceOfElementLocated(openSlide));
		String LargeCorousal = driver.findElement(openSlide).getText();
		System.out.println(LargeCorousal);
		Assert.assertEquals("294a", LargeCorousal);

		System.out.println("Assertion done.");
		System.out.println("Out of Second function: AdImageClick");
	}

    @Test(dependsOnMethods= {"AdImageClick"})
    public void SwipingLargeImage() throws InterruptedException{
		System.out.println("In Third function: SwipingLargeImage");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		driver.switchTo().frame("native");
		System.out.println("Switched to iframe in 3rd function");
	

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		System.out.println("Starting to swipe");
		Thread.sleep(3000);

		// to open the enlarged image in the next tab
		// driver.findElement(By.xpath("//*[@id=\"air-294-container\"]/div/div[1]/div/div/div/div[1]/img[1]")).click();

		// Defining WebElements for the Images in carousel
		WebElement slider1 = driver.findElement(Image1);
		WebElement slider2 = driver.findElement(Image2);
		WebElement slider3 = driver.findElement(Image3);

		Actions action = new Actions(driver);
		action.clickAndHold(slider1);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		action.moveToElement(slider2).release();
		action.build().perform();


		// to check if action is logged in console
		driver.switchTo().parentFrame();

		wait.until(ExpectedConditions.visibilityOfElementLocated(LeftSwipe));
		String LS = driver.findElement(LeftSwipe).getText();
		System.out.println(LS);
		Assert.assertEquals("Large_Slider_Swipe_Left", LS);

		System.out.println("Left Swipe Assertion done.");

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		driver.switchTo().frame("native");
		System.out.println("Switched to iframe Again");

		// -----------------------------------------------------------------
		// WebElement LargeCrousalOpen = driver.findElement(openSlide);
		// Assert.assertEquals(true, LargeCrousalOpen.isDisplayed());

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));// 10
		System.out.println("Left Swipe Done.1");

		Actions action2 = new Actions(driver);
		action2.clickAndHold(slider2);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		action2.moveToElement(slider3).release();
		action2.build().perform();

		System.out.println("Left Swipe Done.2");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));// 10

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		System.out.println("Out of  Third function: SwipingLargeImage");
	}

    @Test(dependsOnMethods= {"SwipingLargeImage"})
    public void ClosingBigCrouselAndAd() throws InterruptedException {
		System.out.println("In Last function: ClosingBigCrouselAndAd");
		// Close the corousal
		Thread.sleep(5000);
		wait.until(ExpectedConditions.presenceOfElementLocated(minimizeSlider));
		wait.until(ExpectedConditions.elementToBeClickable(minimizeSlider));
		driver.findElement(minimizeSlider).click();
		System.out.println("Minimized.");
		// ------------------------------------------------------------------
		// to check if action is logged in console
		driver.switchTo().parentFrame();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

		wait.until(ExpectedConditions.presenceOfElementLocated(minimizevalue));
		String close = driver.findElement(minimizevalue).getText();
		System.out.println(close);
		Assert.assertEquals("294b", close);

		System.out.println("Minimize Assertion done.");

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		// -------------------------------------------------------------------

		// Close the console
		wait.until(ExpectedConditions.presenceOfElementLocated(closeConsole));
		wait.until(ExpectedConditions.elementToBeClickable(closeConsole));
		driver.findElement(closeConsole).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

		// close Ad
		driver.switchTo().frame("native");
		driver.findElement(closeAd).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		
		// Checking if Ad is there or not in the mobile UI
		boolean adDisplayed = driver.findElement(AfterAdClose).isDisplayed();
		
		if (adDisplayed) {
			System.out.println("Ad is not there");
		} else {
			System.out.println("Ad is there");
		}

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(50));

		// Note: smallImage1 crousel stops after one swipe while doing it manually too.(browser: Firefox and chrome.)
		/*
		 * WebElement s1 = driver.findElement(smallImage1);
		 * WebElement s2 = driver.findElement(smallImage2);
		 * WebElement s3 = driver.findElement(smallImage3);
		 * WebElement s4 = driver.findElement(smallImage4);
		 * 
		 * Actions Saction = new Actions(driver);
		 * Saction.clickAndHold(s1);
		 * Saction.moveToElement(s2).release();
		 * Saction.build().perform();
		 * 
		 * driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		 * System.out.println("Swipe Done.S-1");
		 * 
		 * Actions Saction2 = new Actions(driver);
		 * Saction2.clickAndHold(s2);
		 * Saction2.moveToElement(s3).release();
		 * Saction2.build().perform();
		 * 
		 * driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		 * System.out.println("Swipe Done.S-2");
		 * 
		 * //right swipe
		 * Actions Saction3 = new Actions(driver);
		 * Saction3.clickAndHold(s3);
		 * Saction3.moveToElement(s4).release();
		 * Saction3.build().perform();
		 * 
		 * System.out.println("Swipe Done. S-3");
		 * driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		 */

		System.out.println("Out of Last function: ClosingBigCrouselAndAd");
	}


   @AfterSuite
   public void teardown()
  {
    driver.quit();
  }
}
